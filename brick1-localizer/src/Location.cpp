#include "zmq.hpp"
#include <windows.h>  
#include <stdio.h>
#include <stdlib.h>
#include <iostream>    
#include <opencv2/opencv.hpp> 
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;  
using namespace cv; 

void localization(Mat &colorImage, Mat &depthImage,Mat &roiImage);

int main()
{   
	// Prepare ZMQ context and subscibe to the Kinect data
	zmq::context_t context(1);
	zmq::socket_t subscriber(context,ZMQ_SUB);
	subscriber.connect("tcp://localhost:5559");
	subscriber.setsockopt(ZMQ_SUBSCRIBE,"",0);

	// Prepare ZMQ context to publish location data
	zmq::context_t(2);
	zmq::socket_t publisher(context,ZMQ_PUB);
	publisher.bind("tcp://*:5557");

	//--Initialize visualization information//
	Mat colorImage(480,640,CV_8UC3);					
	Mat depthImage(480,640,CV_8UC1);	
	Mat roiImage(colorImage,Range(200,350),Range(300, 380));	//Range(rows,cols)
						
	namedWindow("Sub-ColorImage", CV_WINDOW_AUTOSIZE);   
	namedWindow("ROI Region", CV_WINDOW_AUTOSIZE);
	
	// Program loop
	while(1)                                             
	{  	
	
		zmq::message_t colormessage;
		zmq::message_t depthmessage;
		subscriber.recv(&colormessage);
		subscriber.recv(&depthmessage);
		memcpy(colorImage.data,colormessage.data(),colormessage.size());
		memcpy(depthImage.data,depthmessage.data(),depthmessage.size());

		// TODO: fill the localization method with 
		localization(colorImage,depthImage,roiImage);

		imshow("Sub-ColorImage",colorImage);
		imshow("ROI Region",roiImage);

		// publish the resulting region of interest (ROI) to all subscribers
		int len1 = roiImage.total()*roiImage.channels();
		zmq::message_t roimessage(len1);
		memcpy(roimessage.data(),roiImage.data,len1);
		bool rc1 = publisher.send(roimessage);
		
		if ('q' == waitKey(10)) //press "q" to exit
		{                
			break;
		}
	}
	return 0;
}


void localization(Mat &colorImage, Mat &depthImage, Mat &roiImage)
{

	Rect bounding_rect(260, 195, 120, 90); // TODO: for example: compute a bounding box
		
	Mat newimg(colorImage, bounding_rect);   //region of interest
	resize(newimg,roiImage,Size(224, 224),0,0,INTER_LANCZOS4);   //resize the ROI
	rectangle(colorImage, bounding_rect, Scalar(0, 255, 255), 0.3, 8, 0);   //draw the bouding box in colorImage

}