#include"zmq.hpp"
#include <windows.h>  
#include <stdio.h>
#include <stdlib.h>
#include <iostream>    
#include <opencv2/opencv.hpp> 
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <vector>
#include <fstream>

using namespace std;
using namespace cv;


int main()
{		

	// Prepare ZMQ context to receive a ROI from localization
	zmq::context_t context(2);
	zmq::socket_t subscriber(context,ZMQ_SUB);
	subscriber.connect("tcp://localhost:5557");
	subscriber.setsockopt(ZMQ_SUBSCRIBE,"",0);
	
	// Preprare another ZMQ context to publish your classification result
	zmq::context_t(3);
	zmq::socket_t publisher(context,ZMQ_PUB);
	publisher.bind("tcp://*:5556");

	RNG rng(12345); // TODO: I am sure there are better ways to identify a product...

	namedWindow("RoiImage",CV_WINDOW_AUTOSIZE);
	Mat roiImage(224,224,CV_8UC3);

	// Program loop
	while(1)
	{
		// fetch data from ZMQ transmission
		zmq::message_t roimessage;
		subscriber.recv(&roimessage);
		memcpy(roiImage.data,roimessage.data(),roimessage.size());
		imshow("RoiImage",roiImage);
	
		int prediction=waitKey(8); // TODO: find a better signal than pressing a key
		if(prediction=='p')
		{

			//TODO:Implement classification based on your algorithm.	
			int prediction = rng.next() % 4; // Please do something more sophisticated than this...
			printf("Prediction:%i\n", prediction);

		
			// On identification, publish the result
			int flags=0;
			zmq::message_t msg(sizeof(int));
			memcpy(msg.data(), &prediction, sizeof(int));
			publisher.send(msg,flags);

		}
	
		if ('q' == waitKey(8)){
			break;
		}
    
	}

}

